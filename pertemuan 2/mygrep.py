#mygrep.py
import sys

script = sys.argv[0]

def print_usage():
    sys.exit(f'Usage : python {script} pattern')

def main(argv):
    if not len(argv) == 1:
        print_usage()
    pattern = argv[0]
    check_input = open(argv[1]) 
    for line in check_input:
        if pattern in line :
            print(line.strip())
    

if __name__ == '__main__':
    main(sys.argv[1:])