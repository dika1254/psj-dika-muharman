#testgrep.py
import sys

script = sys.argv[0]
#fungsi untuk menampilkan text error
def print_usage():
    sys.exit(f'Usage : python {script} pattern')
# fungsi main dengan parameter argv
def main(argv):
    # Jika argumen di depan itu sama dengan 0
    if len(argv) == 0:
        # Akan di tampilkan pesan error
        print_usage()
    # Variabel cek input dengan default value sys.stdin. input dari pipeline linux
    check_input = sys.stdin
    # Jika argumen lebih dari 2 maka check input akan di ambil dari argument kedua yang berupa lokasi file
    if len(argv) > 2:
        # Cek jika file ada atau tidak 
        try:
            check_input = open(argv[1])
        except :
            sys.exit(f'File not found')
    # Argumen pertama adalah data yang di cari
    pattern = argv[0]
    # looping isi file dari check input
    for line in check_input:
        # Jika argumen pertama ketemu maka akan di print
        if pattern in line :
            print(line.strip())

# Untuk lebih lanjut bisa cek https://www.freecodecamp.org/news/whats-in-a-python-s-name-506262fe61e8/
if __name__ == '__main__':
    main(sys.argv[1:])